from django.shortcuts import render

from django.http import HttpResponse
# Create your views here.


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")


def kenn(request):
    return HttpResponse("This is Kenn")


def detail(request, question_id):
    # return HttpResponse("You're looking at question %s." % question_id)
    return HttpResponse(("You're looking at question ", question_id))


def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)


def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)

def sum(request, number1, number2):
    return HttpResponse(('The sum of ', number1, " and ", number2, "is ", (number1+number2from )))