from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('kenn/', views.kenn),
    path('<int:question_id>/', views.detail),
    # ex: /polls/5/results/
    path('<int:question_id>/results/', views.results),
    # ex: /polls/5/vote/
    path('<int:question_id>/vote/', views.vote),
    path('<int:number1>/<int:number2>/', views.sum, name='sum'),
]